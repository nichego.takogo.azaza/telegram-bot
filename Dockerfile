FROM golang:alpine AS builder
WORKDIR /build
COPY . .
RUN go build -o main

FROM alpine
WORKDIR /app
COPY --from=builder /build/main .
COPY --from=builder /build/.env .env
CMD ["/app/main"]