package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/lpernett/godotenv"

	_ "github.com/lib/pq"
)

func init() {
	godotenv.Load()
	var err error

	maxAttempts := 15
	waiting := time.Second * 2
	for i := 0; i < maxAttempts; i++ {
		db, err = sql.Open("postgres",
			fmt.Sprintf(
				"postgres://%s:%s@%s/%s?sslmode=disable",
				os.Getenv("POSTGRES_USERNAME"),
				os.Getenv("POSTGRES_PASSWORD"),
				os.Getenv("POSTGRES_HOST"),
				os.Getenv("POSTGRES_DB_NAME"),
			),
		)

		if err == nil && db.Ping() == nil {
			break
		}

		fmt.Printf("Attempts to connect to PostgreSQL %d/%d\n", i+1, maxAttempts)
		db.Close()
		time.Sleep(waiting)
	}
	if err != nil {
		fmt.Println("Fail!")
		log.Println(err)
		return
	}

	fmt.Println("Succes!")

	_, err = db.Exec(tableCreation)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = db.Exec(priceTableCreation)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = db.Exec(historyTableCreation)
	if err != nil {
		log.Println(err)
		return
	}
}

var (
	db            *sql.DB
	tableCreation = `CREATE TABLE IF NOT EXISTS work
(
	date CHARACTER VARYING(30) PRIMARY KEY,
	time CHARACTER VARYING(20) NOT NULL,
	day SMALLINT NOT NULL,
	month SMALLINT NOT NULL,
	year SMALLINT NOT NULL,
	isTaken BOOL NOT NULL,
	username CHARACTER VARYING(30),
	chatID BIGINT
)`
	priceTableCreation = `CREATE TABLE IF NOT EXISTS price
(
	id SMALLINT PRIMARY KEY,
	text TEXT
)`
	historyTableCreation = `CREATE TABLE IF NOT EXISTS history
(
	id SERIAL PRIMARY KEY,
	record CHARACTER VARYING(130)
)`
	standartTimeout = time.Second * 30
)

type window struct {
	Date, Time_ string
	Day, Month  uint8
	Year        uint16
	IsTaken     bool
	Username    string
	ChatID      uint64
}

func createWindow(day, month uint8, year uint16, time_ string) (err error) {
	if time_[0] >= '3' && time_[0] <= '9' {
		time_ = "0" + time_
	}

	var builder strings.Builder
	builder.WriteString(fmt.Sprintf("%d.", year))
	if month < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprintf("%d.", month))
	if day < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprintf("%d ", day))
	builder.WriteString(time_)
	date := builder.String()

	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("INSERT INTO work (date, time, day, month, year, isTaken, username, chatID) VALUES ('%s', '%s', %d, %d, %d, false, '', 0)", date, time_, day, month, year))
	return
}

func getWindow(date string) (window, error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	row := db.QueryRowContext(ctx, fmt.Sprintf("SELECT * FROM work WHERE date = '%s'", date))

	err := row.Err()
	if err != nil {
		return window{}, err
	}

	var w window
	err = row.Scan(&w.Date, &w.Time_, &w.Day, &w.Month, &w.Year, &w.IsTaken, &w.Username, &w.ChatID)

	return w, err
}

func getWindowsByUsername(username string) ([]window, error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	rows, err := db.QueryContext(ctx, fmt.Sprintf("SELECT * FROM work WHERE username = '%s'", username))
	if err != nil {
		return nil, err
	}

	w := make([]window, 0, 32)
	if rows != nil {
		for rows.Next() {
			unit := window{}
			err = rows.Scan(&unit.Date, &unit.Time_, &unit.Day, &unit.Month, &unit.Year, &unit.IsTaken, &unit.Username, &unit.ChatID)
			if err != nil {
				return w, err
			}
			w = append(w, unit)
		}
	}

	return w, nil
}

func updateWindow(date string, newData window) (err error) {
	w, err := getWindow(date)
	if err != nil {
		return
	}

	if len(newData.Date) > 0 {
		w.Date = newData.Date
	}
	if len(newData.Time_) > 0 {
		w.Time_ = newData.Time_
	}
	if newData.Day > 0 {
		w.Day = newData.Day
	}
	if newData.Month > 0 {
		w.Month = newData.Month
	}
	if newData.Year > 0 {
		w.Year = newData.Year
	}
	if len(newData.Username) > 0 {
		w.Username = newData.Username
	}
	if newData.ChatID > 0 {
		w.ChatID = newData.ChatID
	}

	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx,
		fmt.Sprintf(
			"UPDATE work SET date = '%s', time = '%s', day = %d, month = %d, year = %d, username = '%s', chatID = %d WHERE date = '%s'",
			w.Date, w.Time_, w.Day, w.Month, w.Year, w.Username, w.ChatID, date,
		),
	)
	return
}

func deleteWindow(date string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("DELETE FROM work WHERE date = '%s'", date))
	return
}

func setAsFree(date string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("UPDATE work SET isTaken = false, username = '' WHERE date = '%s'", date))
	return
}

func setAsTaken(date string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("UPDATE work SET isTaken = true WHERE date = '%s'", date))
	return
}

func isTakenWindowsList(b bool) []window {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	rows, _ := db.QueryContext(ctx, fmt.Sprintf("SELECT * FROM work WHERE isTaken = %t", b))
	defer cancel()

	var err error
	w := make([]window, 0, 32)
	if rows != nil {
		for rows.Next() {
			unit := window{}
			err = rows.Scan(&unit.Date, &unit.Time_, &unit.Day, &unit.Month, &unit.Year, &unit.IsTaken, &unit.Username, &unit.ChatID)
			if err == nil {
				w = append(w, unit)
			}
		}
	}
	return w
}

func list() []window {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	rows, _ := db.QueryContext(ctx, "SELECT * FROM work")
	defer cancel()

	var err error
	w := make([]window, 0, 32)
	if rows != nil {
		for rows.Next() {
			unit := window{}
			err = rows.Scan(&unit.Date, &unit.Time_, &unit.Day, &unit.Month, &unit.Year, &unit.IsTaken, &unit.Username, &unit.ChatID)
			if err == nil {
				w = append(w, unit)
			}
		}
	}
	return w
}

func setPrice(text string) (err error) {
	if text == "отмена" {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	_, err = db.ExecContext(ctx, fmt.Sprintf("INSERT INTO price (id, text) VALUES (1, '%s')", text))
	cancel()
	if err != nil {
		ctx, cancel = context.WithTimeout(context.Background(), standartTimeout)
		_, err = db.ExecContext(ctx, fmt.Sprintf("UPDATE price SET text = '%s' WHERE id = 1 ", text))
		cancel()
	}
	return
}

func getPrice() (text string) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	row := db.QueryRowContext(ctx, "SELECT text FROM price WHERE id = 1")
	cancel()
	row.Scan(&text)
	return
}

func addHistoryRecord(isAppointment bool, firstName, lastName, username, day, month, year, time_ string) (err error) {
	now := time.Now()

	var builder strings.Builder
	var logMin, logHour, logDay, logMonth string
	if now.Minute() < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprint(now.Minute()))
	logMin = builder.String()
	builder.Reset()

	if now.Hour() < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprint(now.Hour()))
	logHour = builder.String()
	builder.Reset()

	if now.Day() < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprint(now.Day()))
	logDay = builder.String()
	builder.Reset()

	if now.Month() < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprint(now.Month()))
	logMonth = builder.String()
	builder.Reset()

	var action string
	if isAppointment {
		action = "Запись"
	} else {
		action = "Отмена"
	}

	record := fmt.Sprintf(
		"%s.%s.%d %s:%s %s %s %s (@%s) %s.%s.%s %s",
		logDay, logMonth, now.Year(), logHour, logMin, action, firstName, lastName, username, day, month, year, time_,
	)

	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	_, err = db.ExecContext(ctx, fmt.Sprintf("INSERT INTO history (record) VALUES ('%s')", record))
	return
}

func getHistory() ([]string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), standartTimeout)
	defer cancel()
	rows, err := db.QueryContext(ctx, "SELECT record FROM history")
	if err != nil {
		return nil, err
	}

	records := make([]string, 0, 256)
	for rows.Next() {
		var record string
		err = rows.Scan(&record)
		if err != nil {
			return nil, err
		}
		records = append(records, record)
	}

	return records, nil
}

/*func migrationFromWork2ToWork() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	rows, err := db.QueryContext(ctx, "SELECT * FROM work2")
	if err != nil {
		return err
	}

	oldWindows := make([]window, 0, 64)
	if rows != nil {
		for rows.Next() {
			unit := window{}
			err = rows.Scan(&unit.Date, &unit.Time_, &unit.Day, &unit.Month, &unit.Year, &unit.IsTaken, &unit.Username, &unit.ChatID)
			if err != nil {
				return err
			}
			oldWindows = append(oldWindows, unit)
		}
	}

	for _, wndw := range oldWindows {
		_, err = db.ExecContext(ctx, fmt.Sprintf("INSERT INTO work (date, time, day, month, year, isTaken, username, chatID) VALUES ('%s', '%s', %d, %d, %d, %t, '%s', %d)", wndw.Date, wndw.Time_, wndw.Day, wndw.Month, wndw.Year, wndw.IsTaken, wndw.Username, wndw.ChatID))
		if err != nil {
			return err
		}

		_, err = db.ExecContext(ctx, fmt.Sprintf("DELETE FROM work2 WHERE date = '%s'", wndw.Date))
		if err != nil {
			return err
		}
	}

	rows, err = db.QueryContext(ctx, "SELECT * FROM work2")
	if err != nil {
		return err
	}

	oldWindows = make([]window, 0, 16)
	if rows != nil {
		for rows.Next() {
			unit := window{}
			err = rows.Scan(&unit.Date, &unit.Day, &unit.Month, &unit.Year, &unit.IsTaken, &unit.Username, &unit.ChatID)
			if err != nil {
				return err
			}
			oldWindows = append(oldWindows, unit)
		}
	}

	if len(oldWindows) == 0 {
		_, err = db.ExecContext(ctx, "DROP TABLE work")
		if err != nil {
			return err
		}
		log.Println("Table was deleted")
	}
	return nil
}*/
