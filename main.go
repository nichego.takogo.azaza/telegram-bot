package main

import (
	"fmt"
	"log"
	"os"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/lpernett/godotenv"
)

func init() {
	time.Local = TLT
}

var (
	ownersChatID = int64(1080485732)
	TLT          = time.FixedZone("TLT", 4*60*60)
)

func main() {
	godotenv.Load()

	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_TOKEN"))
	if err != nil {
		log.Println("Connect to telegram bot:", err)
		return
	}

	allWorkers(bot)

	updates := bot.GetUpdatesChan(tgbotapi.UpdateConfig{})

	for update := range updates {
		if update.Message != nil {
			err = MessageHandle(bot, update)
		}
		if update.CallbackQuery != nil {
			err = CallbackHandle(bot, update)
		}
		if err != nil {
			var id int64
			var username string

			if update.Message != nil {
				id = update.Message.Chat.ID
				username = update.Message.Chat.UserName
			} else if update.CallbackQuery != nil {
				id = update.CallbackQuery.Message.Chat.ID
				username = update.CallbackQuery.Message.Chat.UserName
			}

			bot.Send(tgbotapi.NewMessage(id, err.Error()))
			bot.Send(tgbotapi.NewMessage(ownersChatID, fmt.Sprintf("Пользователь: @%s\nОшибка: %s", username, err.Error())))
		}
	}
}
