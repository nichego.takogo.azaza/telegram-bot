package main

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func takeFirstDayOfMonth(date time.Time) time.Time {
	if date.Hour() == 23 {
		date = date.Add(time.Hour)
	}

	var i int
	for i = 0; date.Add(time.Hour*-24*time.Duration(i)).Day() > 1; i++ {
	}
	return date.Add(time.Hour * -24 * time.Duration(i))
}

func getMissedWeekDays(weekDay string) int {
	var ans int
	switch weekDay {
	case time.Monday.String():
		ans = int(time.Monday) - 1
	case time.Tuesday.String():
		ans = int(time.Tuesday) - 1
	case time.Wednesday.String():
		ans = int(time.Wednesday) - 1
	case time.Thursday.String():
		ans = int(time.Thursday) - 1
	case time.Friday.String():
		ans = int(time.Friday) - 1
	case time.Saturday.String():
		ans = int(time.Saturday) - 1
	case time.Sunday.String():
		ans = 6
	}
	return ans
}

func getActualWindows(isTaken bool) []button {
	windows := isTakenWindowsList(isTaken)

	dates := make([]string, 0, len(windows))
	for _, v := range windows {
		dates = append(dates, v.Date)
	}

	sort.Strings(dates)

	now := time.Now()
	for i := 0; i < len(dates); i++ {
		datesParts := strings.Split(dates[i], ".")
		if len(datesParts) < 3 {
			log.Println("cant split date into 3 parts")
			return nil
		}
		datesParts[2] = datesParts[2][:2]

		intSlc := make([]int, 0, 3)
		for _, v := range datesParts {
			got, err := strconv.Atoi(v)
			if err != nil {
				log.Println(err)
			}
			intSlc = append(intSlc, got)
		}

		if intSlc[0] > now.Year() {
			dates = dates[i:]
			i = len(dates)
		} else if intSlc[0] == now.Year() {
			if intSlc[1] > int(now.Month()) {
				dates = dates[i:]
				i = len(dates)
			} else if intSlc[1] == int(now.Month()) && intSlc[2] > now.Day() {
				dates = dates[i:]
				i = len(dates)
			}
		}

		if i == len(dates)-1 {
			dates = []string{}
		}
	}

	for i, v := range dates {
		for i2, v2 := range windows {
			if v2.Date == v {
				windows[i], windows[i2] = windows[i2], windows[i]
				break
			}
		}
	}
	windows = windows[:len(dates)]

	btns := make([]button, 0, len(windows))
	var day string
	row := 0
	for _, ww := range windows {
		parts := strings.Split(ww.Date, " ")
		if day != parts[0] {
			day = parts[0]
			row++
			dp := strings.Split(day, ".")
			btns = append(btns, button{Text: fmt.Sprintf("%s.%s.%s", dp[2], dp[1], dp[0]), Data: "empty", Row: row})
		}
		btns = append(btns, button{Text: parts[1], Data: "Ask:Get:" + ww.Date, Row: row})
	}
	return btns
}

func createDateString(day, month uint8, year uint16, time_ string) string {
	var builder strings.Builder
	if day < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprintf("%d.", day))
	if month < 10 {
		builder.WriteString("0")
	}
	builder.WriteString(fmt.Sprintf("%d.", month))
	builder.WriteString(fmt.Sprintf("%d ", year))
	builder.WriteString(time_)
	return builder.String()
}

func allWorkers(bot *tgbotapi.BotAPI) {
	workerReminder(bot)
	workerDBCleaner(bot)
}

func workerReminder(bot *tgbotapi.BotAPI) {
	now := time.Now()
	goalTime := time.Date(now.Year(), now.Month(), now.Day(), 19, 0, 0, 0, TLT)

	sub := goalTime.Sub(now)
	if sub < 0 {
		sub += time.Hour * 24
	}
	ch := time.After(sub)

	go func() {
		<-ch
		checkNSendRemind(bot)
		sub = time.Hour * 24
		for {
			ch := time.After(sub)
			<-ch
			checkNSendRemind(bot)
		}
	}()
}

func workerDBCleaner(bot *tgbotapi.BotAPI) {
	now := time.Now()
	goalTime := time.Date(now.Year(), now.Month(), 28, 14, 0, 0, 0, TLT)

	sub := goalTime.Sub(now)
	if sub < 0 {
		sub += time.Hour * 24 * 30
	}
	ch := time.After(sub)

	go func() {
		<-ch
		checkNCleanDB(bot)
		sub = time.Hour * 24 * 30
		for {
			ch := time.After(sub)
			<-ch
			checkNCleanDB(bot)
		}
	}()
}

func checkNSendRemind(bot *tgbotapi.BotAPI) {
	windows := isTakenWindowsList(true)
	now := time.Now()
	for _, w := range windows {
		if w.Day == uint8(now.Day()+1) && w.Month == uint8(now.Month()) && w.Year == uint16(now.Year()) {
			dateNTime := strings.Split(w.Date, " ")
			if len(dateNTime) < 2 {
				log.Println("cant split date and time into 2 parts")
				continue
			}
			bot.Send(tgbotapi.NewMessage(int64(w.ChatID),
				fmt.Sprintf("Напоминаю, что вы записаны на завтра на маникюр\nДата и время: %s", createDateString(w.Day, w.Month, w.Year, dateNTime[1])),
			))
		}
	}
}

func checkNCleanDB(bot *tgbotapi.BotAPI) {
	bot.Send(tgbotapi.NewMessage(ownersChatID, "Началась очистка БД"))
	var count uint16

	windows := list()
	all := len(windows)
	now := time.Now()
	for _, w := range windows {
		date := time.Date(int(w.Year), time.Month(w.Month), int(w.Day), 0, 0, 0, 0, TLT)
		if now.After(date) {
			deleteWindow(w.Date)
			count++
		}
	}

	bot.Send(tgbotapi.NewMessage(ownersChatID, fmt.Sprintf("Очистка БД завершилась, удалено %d/%d записей", count, all)))
}

func isDateInFuture(day, month, year int) bool {
	now := time.Now()
	if year > now.Year() {
		return true
	} else if year == now.Year() {
		if month > int(now.Month()) {
			return true
		} else if month == int(now.Month()) {
			if day > now.Day() {
				return true
			}
		}

	}
	return false
}
