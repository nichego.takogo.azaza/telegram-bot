module gitlab.com/nichego.takogo.azaza/project1

go 1.22.1

require github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1

require github.com/lib/pq v1.10.9

require github.com/lpernett/godotenv v0.0.0-20230527005122-0de1d4c5ef5e
