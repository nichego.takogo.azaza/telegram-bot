package main

import (
	"fmt"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var (
	monthMove      = 0
	lastTimeUpdate = time.Time{}
	currMonth      = time.Time{}
)

type button struct {
	Text, Data string
	Row        int
}

func NewCalendarMarkup() tgbotapi.InlineKeyboardMarkup {
	if lastTimeUpdate.Before(time.Now().Add(time.Hour * -24)) { // Если последнее обращение к календарю было более суток назад
		lastTimeUpdate = time.Now()
		currMonth = takeFirstDayOfMonth(lastTimeUpdate)
	}

	if monthMove != 0 {
		if monthMove > 0 {
			currMonth = currMonth.Add(time.Hour * 24 * 31)
		} else {
			currMonth = currMonth.Add(time.Hour * 24 * -28)
		}
		currMonth = takeFirstDayOfMonth(currMonth)
		monthMove = 0
	}

	y, m, _ := currMonth.Date()

	buttons := make([]button, 0, 64)
	btn := button{
		Text: fmt.Sprintf("%v %v", m, y),
		Data: "empty",
		Row:  1,
	}
	buttons = append(buttons, btn)

	btn.Row++
	btn.Text = "Mo"
	buttons = append(buttons, btn)
	btn.Text = "Tu"
	buttons = append(buttons, btn)
	btn.Text = "We"
	buttons = append(buttons, btn)
	btn.Text = "Th"
	buttons = append(buttons, btn)
	btn.Text = "Fr"
	buttons = append(buttons, btn)
	btn.Text = "St"
	buttons = append(buttons, btn)
	btn.Text = "Su"
	buttons = append(buttons, btn)

	btn.Row++
	btn.Text = " "
	weekDayNumber := 1

	daysCount := getMissedWeekDays(currMonth.Weekday().String())
	for ; daysCount > 0; daysCount-- { // Заполняем пустые клетки перед первым числом в неделе
		buttons = append(buttons, btn)
		weekDayNumber++
	}

	monthName := currMonth.Month().String()

	for i := 0; currMonth.Add(time.Hour*24*time.Duration(i)).Month().String() == monthName; i++ { // Вставляем числа одного месяца
		if weekDayNumber == 8 {
			weekDayNumber = 1
			btn.Row++
		}

		currDate := currMonth.Add(time.Hour * 24 * time.Duration(i))

		btn.Text = fmt.Sprint(currDate.Day())
		btn.Data = fmt.Sprintf("Date:%d.%d.%d", currDate.Day(), currDate.Month(), currDate.Year())
		buttons = append(buttons, btn)

		weekDayNumber++
	}

	btn.Text = " "
	btn.Data = "empty"

	for weekDayNumber < 8 {
		buttons = append(buttons, btn)
		weekDayNumber++
	}

	btn.Row++
	btn.Data = "make move l"
	btn.Text = "←"
	buttons = append(buttons, btn)
	btn.Data = "chosen"
	btn.Text = "Выбранные"
	buttons = append(buttons, btn)
	btn.Data = "make move r"
	btn.Text = "→"
	buttons = append(buttons, btn)

	btn.Row++
	btn.Data = "cancel calendar"
	btn.Text = "Отменить"
	buttons = append(buttons, btn)
	btn.Data = "add time"
	btn.Text = "Подтвердить"
	buttons = append(buttons, btn)

	return NewMarkup(buttons)
}

func NewMarkup(buttons []button) tgbotapi.InlineKeyboardMarkup {
	var max int
	for _, b := range buttons {
		if max < b.Row {
			max = b.Row
		}
	}
	rows := make([][]tgbotapi.InlineKeyboardButton, max)

	var index int
	for _, b := range buttons {
		index = b.Row - 1

		if rows[index] == nil {
			rows[index] = make([]tgbotapi.InlineKeyboardButton, 0, 8)
		}

		rows[index] = append(rows[index], tgbotapi.NewInlineKeyboardButtonData(b.Text, b.Data))
	}

	return tgbotapi.NewInlineKeyboardMarkup(rows...)
}
