package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var (
	initialMsg        = "Привет! Я бот для записи на маникюр"
	chooseTimeMsg     = "Напиши окошки через запятую без пробелов.\nОни применятся к выбранным тобой дням.\nШаблон:\nтекст,текст,..."
	chosenDatesMap    = make(map[string]struct{}, 64)
	chosenDates       = make([]string, 0, 40)
	chosenTimes       = make([]string, 0, 2)
	toggleReadTime    = false
	readTimeChatID    int64
	toggleReadPrice   = false
	readPriceChatID   int64
	isDelete          = false
	windowsCountLimit = 2
	historyMove       = 0
)

func MessageHandle(bot *tgbotapi.BotAPI, update tgbotapi.Update) (err error) {
	msg := update.Message

	if int64(msg.Date) < time.Now().Unix()-30 {
		return
	}

	switch msg.Text {
	default:
		if toggleReadTime && readTimeChatID == msg.Chat.ID { // Если мы ожидаем ввода времени (используется при создании окошек)
			readTimeChatID = 0
			chosenTimes = strings.Split(msg.Text, ",")

			for date := range chosenDatesMap {
				dateParts := strings.Split(date, ".") // Преобразуем текстовую дату в цифры для использования функции createWindow
				var day, month, year uint64
				if len(dateParts) >= 3 {
					day, err = strconv.ParseUint(dateParts[0], 10, 8)
					if err != nil {
						return
					}
					month, err = strconv.ParseUint(dateParts[1], 10, 8)
					if err != nil {
						return
					}
					year, err = strconv.ParseUint(dateParts[2], 10, 16)
					if err != nil {
						return
					}
				} else {
					return fmt.Errorf("cant split date %s into 3 parts", date)
				}

				for _, tm := range chosenTimes {
					err = createWindow(uint8(day), uint8(month), uint16(year), tm)
					if err != nil {
						return
					}
				}
			}

			toggleReadTime = false

			chosenDatesMap = make(map[string]struct{}, 64)
			chosenDates = make([]string, 0, 40)
			chosenTimes = make([]string, 0, 2)

			return
		} else if toggleReadPrice && readPriceChatID == msg.Chat.ID { // Если мы ожидаем ввода нового прайса
			readPriceChatID = 0
			err = setPrice(msg.Text)
			toggleReadPrice = false
			return
		}
		row := 1

		toSend := tgbotapi.NewMessage(msg.Chat.ID, initialMsg)

		buttons := make([]button, 0, 8)
		buttons = append(buttons,
			button{
				Text: "Прайс",
				Data: "price",
				Row:  row,
			},
			button{
				Text: "Отменить запись",
				Data: "cancel",
				Row:  row,
			},
		)
		row++
		buttons = append(buttons,
			button{
				Text: "Запись",
				Data: "appointment",
				Row:  row,
			},
		)

		row++
		if msg.From.ID == ownersChatID /* || msg.From.ID == 1752475652*/ {
			buttons = append(buttons,
				button{
					Text: "Администрирование",
					Data: "admin",
					Row:  row,
				},
			)
		}

		markup := NewMarkup(buttons)

		toSend.BaseChat.ReplyMarkup = markup

		_, err = bot.Send(toSend)
	}
	return
}

func CallbackHandle(bot *tgbotapi.BotAPI, update tgbotapi.Update) (err error) {
	cb := update.CallbackQuery

	switch cb.Data {
	case "list":
		bot.Send(tgbotapi.NewCallback(cb.ID, "")) // Для того чтобы кнопки не грузились (мелькание)
		_, err = bot.Send(tgbotapi.NewMessage(cb.Message.Chat.ID, fmt.Sprintf("%v", list())))
	case "admin":
		historyMove = 0
		buttons := []button{
			{
				Text: "Окошки",
				Data: "windows",
				Row:  1,
			},
			{
				Text: "Прайс",
				Data: "edit price",
				Row:  1,
			},
			{
				Text: "История",
				Data: "history",
				Row:  2,
			},
			{
				Text: "list",
				Data: "list",
				Row:  2,
			},
		}

		markup := NewMarkup(buttons)

		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Администрирование", markup))
	case "windows":
		buttons := []button{
			{
				Text: "Открыть новые",
				Data: "create windows",
				Row:  1,
			},
			{
				Text: "Удалить существующие",
				Data: "delete windows",
				Row:  1,
			},
		}

		markup := NewMarkup(buttons)

		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Окошки", markup))
	case "make move l":
		monthMove -= 2
		fallthrough
	case "make move r":
		monthMove++
		fallthrough
	case "create windows":
		markup := NewCalendarMarkup()

		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Выберите нужные даты", markup))
	case "cancel calendar":
		chosenDatesMap = make(map[string]struct{}, 64)
		chosenDates = make([]string, 0, 40)
		_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Создание окошек прекращено"))
	case "chosen":
		bot.Send(tgbotapi.NewCallback(cb.ID, "")) // Для того чтобы кнопки не грузились (мелькание)
		_, err = bot.Send(tgbotapi.NewMessage(cb.Message.Chat.ID, fmt.Sprintf("%v", chosenDates)))
	case "add time":
		readTimeChatID = cb.Message.Chat.ID
		toggleReadTime = true
		_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, chooseTimeMsg))
	case "delete windows":
		isDelete = true
		buttonsFree := getActualWindows(false)
		buttonsTaken := getActualWindows(true)

		if len(buttonsFree)+len(buttonsTaken) == 0 {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Свободных окошек, которые можно было бы удалить на данный момент нет"))
			return
		}

		var row int
		for _, b := range buttonsFree {
			if row < b.Row {
				row = b.Row
			}
		}
		row++
		buttonsFree = append(buttonsFree,
			button{
				Text: "Посмотреть занятые",
				Data: "delete windows taken",
				Row:  row,
			},
		)

		markup := NewMarkup(buttonsFree)
		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Выберите даты для удаления", markup))
	case "delete windows taken":
		isDelete = true
		buttonsTaken := getActualWindows(true)

		if len(buttonsTaken) == 0 {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Занятых окошек, которые можно было бы удалить на данный момент нет"))
			return
		}

		var row int
		for _, b := range buttonsTaken {
			if row < b.Row {
				row = b.Row
			}
		}
		row++
		buttonsTaken = append(buttonsTaken,
			button{
				Text: "Посмотреть свободные",
				Data: "delete windows",
				Row:  row,
			},
		)

		markup := NewMarkup(buttonsTaken)
		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Выберите даты для удаления", markup))
	case "edit price":
		readPriceChatID = cb.Message.Chat.ID
		_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
			"Введите текст который будет выводится клиентам при нажатии на кнопку \"Прайс\"\nЕсли не хотите менять текст прайса напишите \"отмена\" без кавычек"))
		if err != nil {
			return
		}
		toggleReadPrice = true
	case "make history move l":
		historyMove -= 2
		fallthrough
	case "make history move r":
		historyMove++
		fallthrough
	case "history":
		var history []string
		history, err = getHistory()
		if err != nil {
			return
		}

		if len(history) == 0 {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "История пуста"))
			return
		}

		if historyMove < 0 {
			historyMove = 0
			bot.Send(tgbotapi.NewCallback(cb.ID, "")) // Для того чтобы кнопки не грузились (мелькание)
			return
		}

		pages := make([]string, 0, 16)
		var builder strings.Builder
		for i := len(history) - 1; i >= 0; i-- {
			builder.WriteString(history[i])
			if len(history)-1-i%30 == 0 && len(history)-1 > i || i == 0 {
				pages = append(pages, builder.String())
				builder.Reset()
			} else {
				builder.WriteString("\n")
			}
		}

		if historyMove > len(pages)-1 {
			historyMove = len(pages) - 1
			bot.Send(tgbotapi.NewCallback(cb.ID, "")) // Для того чтобы кнопки не грузились (мелькание)
			return
		}

		row := 1
		buttons := []button{
			{
				Text: "Пред.",
				Data: "make history move l",
				Row:  row,
			},
			{
				Text: "След.",
				Data: "make history move r",
				Row:  row,
			},
		}

		markup := NewMarkup(buttons)

		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, pages[historyMove], markup))
	case "appointment":
		windows := isTakenWindowsList(true)
		var count int
		for _, v := range windows {
			if isDateInFuture(int(v.Day), int(v.Month), int(v.Year)) && v.Username == cb.From.UserName {
				count++
			}
		}

		if count >= windowsCountLimit {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
				fmt.Sprintf("Вы достигли лимита одновременно активных записей %d из %d", count, windowsCountLimit),
			))
			return
		}

		isDelete = false
		buttons := getActualWindows(false)
		if len(buttons) == 0 {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Свободных окошек на данный момент нет"))
		} else {
			markup := NewMarkup(buttons)
			_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Выберите дату", markup))
		}
	case "cancel":
		var windows []window
		windows, err = getWindowsByUsername(cb.From.UserName)
		if err != nil {
			return
		}

		row := 1
		buttons := make([]button, 0, len(windows))
		for _, v := range windows {
			if v.IsTaken {
				if isDateInFuture(int(v.Day), int(v.Month), int(v.Year)) {
					buttons = append(buttons,
						button{
							Text: createDateString(v.Day, v.Month, v.Year, v.Time_),
							Data: fmt.Sprintf("Cancel:%s", v.Date),
							Row:  row,
						},
					)
					row++
				}
			}
		}

		if len(buttons) == 0 {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "У вас нет занятых окошек"))
			return
		}

		markup := NewMarkup(buttons)
		_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID, "Выберите какую запись хотите отменить", markup))
	case "price":
		text := getPrice()
		if len(text) == 0 {
			bot.Send(tgbotapi.NewDeleteMessage(cb.Message.Chat.ID, cb.Message.MessageID))
			err = fmt.Errorf("пустой прайс лист")
		} else {
			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, text))
		}
	default:
		bot.Send(tgbotapi.NewCallback(cb.ID, "")) // Для того чтобы кнопки не грузились (мелькание)
		date := cb.Data
		if len(date) >= 5 && date[:5] == "Date:" { // Записываем окошки для их дальнейшего создания
			date = date[5:]

			if _, ok := chosenDatesMap[date]; !ok {
				chosenDatesMap[date] = struct{}{}
				chosenDates = append(chosenDates, date)
			} else {
				delete(chosenDatesMap, date)
				for i := len(chosenDates) - 1; i >= 0; i-- {
					if chosenDates[i] == date {
						chosenDates = append(chosenDates[:i], chosenDates[i+1:]...)
						break
					}
				}
			}
		} else if len(date) >= 4 && date[:4] == "Get:" { // Занятие или удаление окошек
			date = date[4:]

			if !isDelete {
				var w window
				w, err = getWindow(date)
				if err != nil {
					return
				}

				if w.IsTaken {
					_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Кажется это окошко уже кто-то занял("))
					return
				}
			}

			dateAndTime := strings.Split(date, " ")
			if len(dateAndTime) < 2 {
				return fmt.Errorf("dateAndTime's len is under 2")
			}
			datesParts := strings.Split(dateAndTime[0], ".")
			if len(datesParts) < 3 {
				return fmt.Errorf("datesParts's len is under 3")
			}

			if isDelete {
				if cb.Message.Chat.ID != ownersChatID {
					_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID, "Запись не прошла, попробуйте еще раз"))
					return
				}
				err = deleteWindow(date)
				if err != nil {
					return
				}
				_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
					fmt.Sprintf(
						"Вы удалили окошко на %s.%s.%s в %s",
						datesParts[2], datesParts[1], datesParts[0], dateAndTime[1],
					)))
				if err != nil {
					return
				}
			} else {
				err = setAsTaken(date)
				if err != nil {
					return
				}
				err = updateWindow(date, window{Username: cb.From.UserName, ChatID: uint64(cb.Message.Chat.ID)})
				if err != nil {
					return
				}

				_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
					fmt.Sprintf(
						"Вы записаны на %s.%s.%s в %s",
						datesParts[2], datesParts[1], datesParts[0], dateAndTime[1],
					)))
				if err != nil {
					return
				}
				_, err = bot.Send(tgbotapi.NewMessage(ownersChatID, fmt.Sprintf(
					"%s %s (@%s) только что записалась на дату %s.%s.%s в %s",
					cb.From.FirstName, cb.From.LastName, cb.From.UserName, datesParts[2], datesParts[1], datesParts[0], dateAndTime[1],
				)))
				if err != nil {
					return
				}

				err = addHistoryRecord(true, cb.From.FirstName, cb.From.LastName, cb.From.UserName, datesParts[2], datesParts[1], datesParts[0], dateAndTime[1])
			}
		} else if len(date) >= 7 && date[:7] == "Cancel:" { // Отмена записи
			date = date[7:]
			setAsFree(date)

			dateAndTime := strings.Split(date, " ")
			if len(dateAndTime) < 2 {
				return fmt.Errorf("dateAndTime's len is under 2")
			}
			datesParts := strings.Split(dateAndTime[0], ".")
			if len(datesParts) < 3 {
				return fmt.Errorf("datesParts's len is under 3")
			}

			_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
				fmt.Sprintf(
					"Вы отменили запись на %s.%s.%s в %s",
					datesParts[2], datesParts[1], datesParts[0], dateAndTime[1],
				)))
			if err != nil {
				return
			}
			_, err = bot.Send(tgbotapi.NewMessage(ownersChatID, fmt.Sprintf(
				"Отмена! %s %s (@%s) только что отменила запись на %s.%s.%s в %s",
				cb.From.FirstName, cb.From.LastName, cb.From.UserName, datesParts[2], datesParts[1], datesParts[0], dateAndTime[1],
			)))
			if err != nil {
				return
			}

			err = addHistoryRecord(false, cb.From.FirstName, cb.From.LastName, cb.From.UserName, datesParts[2], datesParts[1], datesParts[0], dateAndTime[1])
		} else if len(date) >= 4 && date[:4] == "Ask:" { // Подтверждение записи и удаления
			if !isDelete {
				windows := isTakenWindowsList(true)
				var count int
				for _, v := range windows {
					if isDateInFuture(int(v.Day), int(v.Month), int(v.Year)) && v.Username == cb.From.UserName {
						count++
					}
				}

				if count >= windowsCountLimit {
					_, err = bot.Send(tgbotapi.NewEditMessageText(cb.Message.Chat.ID, cb.Message.MessageID,
						fmt.Sprintf("Вы достигли лимита одновременно активных записей %d из %d", count, windowsCountLimit),
					))
					return
				}
			}

			dateNTime := strings.Split(date[8:], " ")
			if len(dateNTime) < 2 {
				return fmt.Errorf("cant split date and time into 2 parts")
			}

			dateParts := strings.Split(dateNTime[0], ".")
			if len(dateParts) < 3 {
				return fmt.Errorf("cant split date into 3 parts")
			}

			var day, month, year uint64
			year, err = strconv.ParseUint(dateParts[0], 10, 16)
			if err != nil {
				return
			}
			month, err = strconv.ParseUint(dateParts[1], 10, 8)
			if err != nil {
				return
			}
			day, err = strconv.ParseUint(dateParts[2], 10, 8)
			if err != nil {
				return
			}

			buttons := make([]button, 0, 8)
			buttons = append(buttons,
				button{
					Text: "Да",
					Data: date[4:],
					Row:  1,
				},
				button{
					Text: "Нет",
					Data: "appointment",
					Row:  1,
				},
			)

			markup := NewMarkup(buttons)

			_, err = bot.Send(tgbotapi.NewEditMessageTextAndMarkup(cb.Message.Chat.ID, cb.Message.MessageID,
				fmt.Sprintf(
					"Дата, которую вы выбрали: %s",
					createDateString(uint8(day), uint8(month), uint16(year), dateNTime[1]),
				),
				markup,
			))
		}
	}
	return
}
